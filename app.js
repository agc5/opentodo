var bodyParser = require("body-parser")
var express = require("express");
var fs = require("fs");
var http = require("http");

var config = JSON.parse(fs.readFileSync(__dirname + "/config.json", "utf8"));

console.log(config)

var app = express();

var router = express.Router();

app.use(express.static(__dirname + "/public"));
app.use(bodyParser.json());

app.set("views", __dirname + "/views");
app.set("view engine", "jade");



app.get("/", function(request, response){
	response.render("index", {"message" : "I absolutely love this", "title" : config["title"], "sites" : config["sites"]});
});

app.post("/", function(request, response) {
	if(request.body.clicked)
	{
		console.log(request.body.clicked);
		response.end(config["sites"][request.body.clicked][0]);
	}
	response.end();
});

http.createServer(app).listen(80);
console.log("App server running on port 80");